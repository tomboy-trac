using System;
using Tomboy;

namespace Tomboy.Trac
{
	public class TracPreferenceFactory : AddinPreferenceFactory
	{
		public override Gtk.Widget CreatePreferenceWidget ()
		{
			return new TracPreferences ();
		}
	}
}
