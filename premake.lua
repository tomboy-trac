project.name = "Tomboy.Trac"
--[[
  Title  : premake.lua
  Author : Thomas Harning Jr.
]]
project.libdir = "lib"
project.bindir = "bin"

package = newpackage()
package.kind = "dll"
package.language = "c#"

local function exec(cmd)
	local f = assert(io.popen(cmd))
	local data = assert(f:read('*a'))
	f:close()
	return data
end
package.links = {
	"System",
--	"Mono.Addins",
	"Mono.Posix",
--	"Tomboy",
	exec('pkg-config tomboy-addins --libs')
}

package.linkoptions = { "-pkg:gtk-sharp", "-pkg:tomboy-addins" }

package.files = {
	matchrecursive(
		"src/*.cs"
	)
}
